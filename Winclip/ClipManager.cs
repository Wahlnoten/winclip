﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Winclip
{
    public class ClipManager : Form
    {
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SetClipboardViewer(IntPtr hWndNewViewer);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool ChangeClipboardChain(IntPtr hWndRemove, IntPtr hWndNewNext);

        IntPtr clipBoardViewer;

        public Dictionary<int, object> clipBoard = new Dictionary<int, object>();
        int itt = 0;

        public ClipManager()
        {
            RegisterClipboardViewer();
        }

        ~ClipManager()
        {
            UnregisterClipboardViewer();
        }

        private void RegisterClipboardViewer()
        {
            clipBoardViewer = SetClipboardViewer(this.Handle);
        }

        private void UnregisterClipboardViewer()
        {
            ChangeClipboardChain(this.Handle, clipBoardViewer);
        }

        protected override void WndProc(ref Message m)
        {
            switch(m.Msg)
            {
                case 0x0308:
                    copyClip();
                    break;
                case 0x030D:
                    ChangeChainCalled(m);
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }

        private void ChangeChainCalled(Message m)
        {
            if (m.WParam == clipBoardViewer)
            {
                clipBoardViewer = m.LParam;
            }
        }

        public void copyClip()
        {
            IDataObject clipboardData = new DataObject();

            try
            {
                clipboardData = Clipboard.GetDataObject();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Exception: {ex.Message}");
                return;
            }

            if (clipboardData.GetDataPresent(DataFormats.Text))
            {
                var str = (string)clipboardData.GetData(DataFormats.Text);
                clipBoard.Add(itt++, str);

                Debug.WriteLine((string)clipboardData.GetData(DataFormats.Text));
            }
        }

        public object getClip(int ojb)
        {
            return 1;
        }
    }
}
